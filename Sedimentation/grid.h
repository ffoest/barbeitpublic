//Guard
#ifndef GRID_INCLUDED
#define GRID_INCLUDED
//Includes
#include "particle.h"
#include "polymer.h"
#include <vector>
#include <random>

constexpr double PI = 4*std::atan(1);

class Grid
{
private:
    int _n_x , _n_y;        // number of x, y grids
   // int _a;               // lattice constant (is now =1)
    Particle * _particles;              // Particle Array
    std::vector<Polymer> _polymers;    // Polymer Array
    int _freeParticleCount; // number of free particles.
    int _polymerCount;      // number of free particles.
    double *** _velocity;   // mean velocity per cell. (x and y component)
    int ** _particlesPerCell;   // particles per cell
    double ** _massPerCell;     // cumulative mass per cell (Particles and Polymers)
    double ** _angles;      // rotation angles for srd.
    double _pressure;       // pressure difference between exits.
    double _x_l, _x_r, _y_l, _y_r;  // limits of Grid (y end is open)
    double _time;                   // timestep
    double _temperature;        // temperature of ghostparticles.
    double*** _meanVelocity;    // mean velocity per cell averaged over measurement time
    std::mt19937 _generator;    // RNG
    double _totalMass;        // total Mass of all Particles and Polymers
    int _steps;               // number of steps simulated
    const double ALPHA = PI*90/180; // 130 degree angle for SRD
    const bool TRUE_RANDOM_ANGLE = false; // determines whether the angle for SRD is chosen in [-pi,pi] or \pm ALPHA


public:
    Grid(int, int, double, double, double); // N_X, N_Y, t, p, T
    ~Grid();
    void populate(std::vector<Particle>); //initialize Grid with (free) particles
    void populate(std::vector<Polymer>);  //initialize Grid with polymers
    void streamingStep();           //streams all particles with their current velocity
    void calculateMeanSpeed();      // calculate speed and number of particles in each cell (includes Ghost particles)
    void srdStep();                 // statistic rotation in each cell
    void accelerationStep();        // accelerates all particles by fixed amount
    void eulerStep();               // euler integration for all polymers in grid
    void reset();                   // reset all Cells (mean velocity and particles per Cell)
    void resetAll();                // reset all Cells and data. (not particles).
    void initAngles();              // init Angles for srd step
    void shiftGrid(double, double); // shift Grid by constant Vector (shift all Particles by constant Vector)
    void simulateFlow(unsigned);         // actual algorithm that does all the work.
    double getTotalMass();          // calculate total mass of all Particles in the Grid.
    void buildFiles();              // text output
    std::vector<Polymer>::iterator getPolymers();
    // Methods used for testing.
    int particlesOutsideOfBoundary(); // count Particles outside of limits for testing
    double totalMomentum(int);        // calculate total momentum of all Particles and Polymers.
};

#endif // GRID_INCLUDED
