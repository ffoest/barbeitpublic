# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as ml
from matplotlib import rcParams
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkorange', 'darkslateblue']

grid  = [20]
param = [0, 0.05, 0.1, 1 ]
press = [0.00, 0.02, 0.04, 0.06, 0.08, 0.10, 0.12, 0.14]
nRows = 2
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nRows, 2, sharex='col', figsize= (8.2, 8)) # figsize in inch (a4 format)
axes = ((ax1, ax2), (ax3, ax4))
for k in range(2):
	for i in range(2):
		# axes labels
		axes[i][0].set_ylabel(r"$\langle N\rangle$") 
		axes[nRows-1][k].set_xlabel(r"$x/a$")
		# plot
		file = "data_hist_" + str(param[2*k+i])+ '_' + str(grid[0]) + ".dat"
		arr = np.loadtxt(file)
		cut = 1
		for j in range(cut, arr.shape[0]):
			xmax = arr.shape[1]
			x = np.linspace(-xmax/4+0.5, xmax/4+0.5, xmax)	
			pltlabel = r"$F = %3.2f \,\frac{ma}{\delta t^2}$"%(press[j])
			axes[i][k].plot(x, arr[j], '-o' ,color = colors[j], markersize=4 , label= pltlabel)
		# text
		#if(k==0):
		text = r'$\frac{\kappa_\mathrm{b}}{m_\mathrm{m}}= %3.2f \,\frac{a^2}{\delta t^2}$'%(param[2*k+i])
		axes[i][k].text(np.min(x)+ xmax/40., np.max(arr), text , fontsize=12)
		# axes limits
		axes[i][k].set_xlim([-arr.shape[1]/4+0.5, arr.shape[1]/4+0.5]) 
		axes[i][k].set_ylim([0, np.max(arr)*1.15]) 
		# ticks
		axes[i][k].xaxis.set_ticks_position('bottom')
		yticks = axes[i][k].yaxis.get_majorticklocs()
		axes[i][k].yaxis.set_ticks(yticks[0:len(yticks)-1])
# legend
handles, labels = ax1.get_legend_handles_labels()	
fig.legend( handles, labels, loc = (0.10, 0.01), ncol =4, prop={'size': 13} )
# layout
fig.tight_layout()
fig.subplots_adjust(left=0.1, bottom=0.18, right=0.95, top=None, hspace=0.01 , wspace=None) 	
# savefig
name = 'plot_sed_histograms'
fig.savefig(name + '.png')
# fig.savefig(name + '.pdf')
fig.savefig('../Latex/Abbildungen/' + name + '.pdf')
plt.close('all')
