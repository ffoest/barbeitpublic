﻿# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as ml
from matplotlib import rcParams
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# param = [0, 0.01, 0.05, 0.1]
param = [0, 0.05, 0.1, 1]
grid = [20]
colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkorange', 'darkslateblue']
for q in range(len(grid)):
	f, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True, figsize= (8,5)) # size not final
	
	for i in range(len(param)):
		file = "data_basic_" + str(param[i]) + '_' + str(grid[q]) + ".dat"
		p, kB, theta, e2e, vx, vy, x, y, mTheta, contour  = np.loadtxt(file, unpack=True)
		pltlabel = r"$\kappa_\mathrm{b}= %3.1f \,\frac{ma^2}{\delta t^2}$"%(param[i])
		ax1.plot(p, e2e/contour, '-o', color = colors[i], markersize=6, label= pltlabel)
		ax2.plot(p, theta, '-o', color = colors[i], markersize=6, label= pltlabel)
		ax3.plot(p, mTheta, '-o', color = colors[i], markersize=6, label= pltlabel)
		# ax3.plot(p, mTheta, '-+', color = colors[i], markersize=6, label= r"$\kappa_b=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
	for ax in [ax1, ax2, ax3]:
		ax.set_xlim([0,np.max(p)*1.03]) 
		ax.set_ylim([0,1]) 
		# ax.yaxis.tick_left()
		ax.xaxis.set_ticks_position('bottom')
		# ax.legend(loc='best',prop={'size':8})
		box = ax.get_position()
		ax.set_position([box.x0, box.y0, box.width * 1.17, box.height])
		# Put a legend to the right of the current axis
		# ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), borderaxespad=0.2, prop={'size':9})
		
	ax1.set_ylabel(r"$\langle L_\mathrm{E}/L_\mathrm{C}\rangle$") 
	ax2.set_ylabel(r"$\langle X_\mathrm{EE}\rangle$") 
	ax3.set_ylabel(r"$\langle X_{\mathrm{mean}}\rangle$")
	ax3.set_xlabel(r"$\frac{F}{m_\mathrm{m}} /\frac{a}{\delta t^2}$") 	
	
	f.tight_layout()
	f.subplots_adjust(hspace = 0.1)
	# fig = ml.pyplot.gcf()
	# fig.set_size_inches(6,3.5)	
	name = 'plot_sed_basic_' + str(grid[q])
	f.savefig(name + '.png')
	#f.savefig(name + '.pdf')
	f.savefig('../Latex/Abbildungen/' + name + '.pdf')
	plt.close('all')	

# for q in range(len(grid)):
	# f = plt.figure(figsize= (8,5))
	# ax = f.add_subplot(111)
	# for i in range(len(param)):
		# file = "data_basic_" + str(param[i]) + '_' + str(grid[q]) + ".dat"
		# p, kB, theta, e2e, vx, vy, x, y, mTheta, contour  = np.loadtxt(file, unpack=True)
		# ax.plot(p, vy, '-o', color = colors[i], markersize=6, label= r"$\kappa_b= %3.1f \,\frac{ma^2}{\delta t^2}$"%(param[i]))
	# ax.set_xlim([0,p.max()*1.03])  
	# ax.set_ylim([0,0.25])  
	# ax.yaxis.tick_left()
	# ax.legend(loc='best', prop={'size':9})
	# # legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0. ,prop={'size':8})
	
	# ax.set_ylabel(r"$\langle v_\mathrm{CM,z}/\frac{a}{\delta t}	\rangle$")
	# ax.set_xlabel(r"$\frac{F}{m_m} / \frac{a}{\delta t^2}$") 	
	# f.tight_layout()
	# f.subplots_adjust(hspace = 0.1)
	# name = 'plot_sed_velocity_' + str(grid[q])
	# f.savefig(name + '.png')
	# f.savefig('../Latex/Abbildungen/' + name + '.pdf')
	# plt.close('all')	