# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as ml
from matplotlib import rcParams
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import *

# param = [0, 0.01, 0.05, 0.1]
param = [0, 0.05, 0.1, 1]
grid = [20]
colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkorange', 'darkslateblue']
N = 9  
M = 10
L = N-1
alpha = np.pi/2
eta_kin = M*(M/((M-1+np.exp(-M))*(1-np.cos(2*alpha))) - 0.5)
eta_col = (1-np.cos(alpha))/(12)*(M-1+np.exp(-M))
print(eta_kin)
print(eta_col)
# eta = 9.6
eta = eta_col + eta_kin
print(eta)

def linear(x, a):
	return a*x
	
for q in range(len(grid)):
	f = plt.figure(figsize= (8,5))
	ax = f.add_subplot(111)
	for i in range(len(param)):
		file = "data_basic_" + str(param[i]) + '_' + str(grid[q]) + ".dat"
		p, kB, theta, e2e, vx, vy, x, y, mTheta, contour  = np.loadtxt(file, unpack=True)
		ax.plot(p*N*M, vy, 'o', color = colors[i], markersize=6, label= r"$\kappa_b= %3.2f \,\frac{ma^2}{\delta t^2}$"%(param[i]))
		popt, pcov = curve_fit(linear, N*M*p, vy)
		(a,) = correlated_values(popt, pcov)
		print(a)
		gamma = 1/a
		R_h = gamma/(6*np.pi*eta)
		ax.plot(p*N*M, linear(p*N*M, a.nominal_value), '--', color = colors[i], markersize=6) 
		#, label= r"$\kappa_b= %3.2f \,\frac{ma^2}{\delta t^2}$"%(param[i])
		print(r'k= {0}, gamma = {1}, R_h = {2}'.format(param[i], gamma, R_h))
	##############	
	par  = 2*np.pi*L*eta/np.log(L)
	perp = 2*par
	print(par)
	print(perp)
	_t = np.linspace(0, np.max(p)*N*M, 1000)
	ax.plot(_t, linear(_t, 1/par), '-' ,color = colors[7], label= 'parallele Ausrichtung')
	ax.plot(_t, linear(_t, 1/perp), '-' ,color = colors[6], label= 'senkrechte Ausrichtung')
	##############
	ax.set_xlim([0,p.max()*1.03*N*M])  
	ax.set_ylim([0,0.25])  
	ax.yaxis.tick_left()
	ax.legend(loc='best', prop={'size':9})
	# legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0. ,prop={'size':8})
	ax.set_ylabel(r"$\langle v_\mathrm{cm,z}/\frac{a}{\delta t}	\rangle$")
	ax.set_xlabel(r"$\frac{F_\mathrm{ges.}}{m} / \frac{a}{\delta t^2}$") 	
	f.tight_layout()
	f.subplots_adjust(hspace = 0.1)
	name = 'plot_sed_velocity_' + str(grid[q])
	f.savefig(name + '.png')
	f.savefig('../Latex/Abbildungen/' + name + '.pdf')
	plt.close('all')