#include "grid.h"
#include <functional>
#include <cstdio>
//#include <Eigen/Dense>

// some Parameters for testing:
const bool TESTING = false;
const bool GHOSTSENABLED = true;

// Streaming in Y direction.

Grid::Grid(int n_x, int n_y, double t, double p, double T): _n_x(n_x), _n_y(n_y), _pressure(p), _time(t), _temperature(T), _generator(43) {
    _velocity = new double ** [_n_x+1];
    _meanVelocity = new double ** [_n_x+1];
    _particlesPerCell = new int * [_n_x+1];
    _massPerCell = new double * [_n_x+1];
    _angles = new double * [_n_x+1];
    for (int i= 0; i< _n_x+1; i++){
        _velocity[i] = new double * [_n_y];
        _meanVelocity[i] = new double * [_n_y];
        _massPerCell[i] = new double [_n_y];
        _particlesPerCell[i] = new int [_n_y];
        _angles[i] = new double [_n_y];
        for (int j= 0; j< _n_y; j++){
            _velocity[i][j] = new double [2];
            _meanVelocity[i][j] = new double [2];
            for(int k=0; k<2; k++){
                _velocity[i][j][k] = 0;
                _meanVelocity[i][j][k] = 0;
            }
        }
    }
   // _particles = new Particle[_freeParticleCount];
    _freeParticleCount = 0;
    _polymerCount = 0;
    _totalMass = 0;
    _steps =0;
    _x_l = 0.5;
    _y_l = 0.5;
    _x_r = _x_l + _n_x  ;
    _y_r = _y_l + _n_y  ;
    resetAll();

}

Grid::~Grid(){
    for (int i=0; i<_n_x+1; i++){
        for (int j=0; j< _n_y; j++){
            delete[] _velocity[i][j];
            delete[] _meanVelocity[i][j];
        }
        delete[] _velocity[i];
        delete[] _meanVelocity[i];
        delete[] _massPerCell[i];
        delete[] _particlesPerCell[i];
        delete[] _angles[i];
    }
    delete[] _velocity;
    delete[] _meanVelocity;
    delete[] _massPerCell;
    delete[] _particlesPerCell;
    delete[] _particles;
    delete[] _angles;
}

void Grid::populate(std::vector<Particle> particles){
    if(_freeParticleCount>0){
        std::printf("Grid already populated with Particles!");
    }else{
        _freeParticleCount = particles.size();
        _particles = new Particle[_freeParticleCount];
        for (unsigned j=0;j<particles.size() ;j++ )
        {
            _particles[j] = particles[j];
        }
        if(particlesOutsideOfBoundary()>0){
            std::printf("Warning: Particles OOB!\n");
        }
    }
}

void Grid::populate(std::vector<Polymer> polymers){
    if(_polymerCount>0){
        std::printf("Grid already populated with Polymers!");
    }else{
        _polymerCount = polymers.size();
        _polymers = std::vector<Polymer>(_polymerCount);
        for (unsigned j=0;j<polymers.size() ;j++ )
        {
            _polymers[j] = polymers[j];
        }
        if(particlesOutsideOfBoundary()>0){
            std::printf("Warning: Particles OOB!\n");
        }
    }
}

void Grid::streamingStep(){
    for (int i=0 ; i<_freeParticleCount ; i++ )   // This currently includes only free particles.
    {
        _particles[i].streaming(_time);
        if(! _particles[i].checkBorders(_x_l, _x_r, 0)){
            _particles[i].reverseDir();
            _particles[i].streaming(_time);
        }
        if(! _particles[i].checkBorders(_y_l, _y_r, 1)){
            _particles[i].verifyPeriodicBoundary(_y_l, _y_r);
        }
    }
}

double Grid::getTotalMass(){
    double mass = 0;
    for (int j=0; j<_freeParticleCount; j++){
        mass += _particles[j].getMass();
    }
    for (int j=0; j<_polymerCount ; j++ ){
        std::vector<Particle>::iterator it=_polymers[j].getChain();
        for(int i=0; i< _polymers[j].getLength(); i++){
            mass += it->getMass();
            it++;
        }
    }
    return mass;
}


void Grid::calculateMeanSpeed(){ //actually calculates mean momentum
    reset();
    if(_totalMass==0){
        _totalMass = getTotalMass();
    }
    double mean= _totalMass/(_n_x*_n_y);

    for (int k=0; k<_freeParticleCount ; k++)
    {
        //lattice constant == 1 -> casting to int returns the cell coordinate.
        int x= int(_particles[k].getPos(0));
        int y= int(_particles[k].getPos(1));
        if(y == _n_y){ //periodic boundary
            y=0;
        }
        _massPerCell[x][y]+= _particles[k].getMass();
        _particlesPerCell[x][y] ++;
        for (int w=0; w<2 ; w++ )
        {
            _velocity[x][y][w] += _particles[k].getVel(w)*_particles[k].getMass();
        }
    }

    for (int k=0; k<_polymerCount ; k++){
        std::vector<Particle>::iterator it=_polymers[k].getChain();
        for (int j=0; j<_polymers[k].getLength(); j++){
            int x= int(it->getPos(0));
            int y= int(it->getPos(1));

            if(y == _n_y){
                y=0;
            }
            _massPerCell[x][y]+= it->getMass();
            _particlesPerCell[x][y] ++;
            for (int w=0; w<2 ; w++ )
            {
                _velocity[x][y][w] += it->getVel(w)* it->getMass();
            }
            it++; // go to next monomer
        }
    }

    for (int i=0; i<_n_x+1; i++){
        for (int j=0; j< _n_y; j++){
            // if there are too few particles at the border, add 0K ghost particles.
            if((i==0 || i==_n_x) && _particlesPerCell[i][j]>0 && _massPerCell[i][j]< mean && GHOSTSENABLED){
                double sigma = std::sqrt((mean - _massPerCell[i][j]) * _temperature);
                std::normal_distribution<double> distribution(0, sigma);
                for (int k=0; k<2; k++){
                    _velocity[i][j][k] += distribution(_generator);
                    _velocity[i][j][k] /= mean;
                }
            }else if(_particlesPerCell[i][j] > 0){
                for (int k=0; k<2; k++){
                    _velocity[i][j][k] /= _massPerCell[i][j];
                }
            }
        }
    }
}

void Grid::accelerationStep(){
    for (int j=0; j<_freeParticleCount ;j++ )
    {
        _particles[j].accelerate(0,_pressure,_time);
    }
}

void Grid::shiftGrid(double x, double y){
    for (int j=0 ;j<_freeParticleCount ;j++ ){
        _particles[j].shift(x,y);
        _particles[j].verifyPeriodicBoundary(_y_l, _y_r);
    }
    for (int k=0; k<_polymerCount ; k++){
        std::vector<Particle>::iterator it=_polymers[k].getChain();
        for (int j=0; j<_polymers[k].getLength(); j++){
            it->shift(x,y);
            it->verifyPeriodicBoundary(_y_l, _y_r);
            it++; // go to next monomer
        }
    }
}

void Grid::srdStep(){
    calculateMeanSpeed();
    initAngles();
    for (int k=0; k<_freeParticleCount ; k++)
    {
        int x= int(_particles[k].getPos(0));
        int y= int(_particles[k].getPos(1));
        if(y == _n_y){
            y=0; //periodic boundary.
        }
        _particles[k].srd(_velocity[x][y][0], _velocity[x][y][1], _angles[x][y]);
    }
    for (int k=0; k<_polymerCount ; k++)
    {
        std::vector<Particle>::iterator it=_polymers[k].getChain();
        for(int i=0; i<_polymers[k].getLength(); i++)
        {
            int x= int(it->getPos(0));
            int y= int(it->getPos(1));
            if(y == _n_y){
                y=0; //periodic boundary.
            }
            it->srd(_velocity[x][y][0], _velocity[x][y][1], _angles[x][y]);
            it++;
        }
    }
}

void Grid::reset(){
    for (int i=0; i<_n_x+1; i++){
        for (int j=0; j<_n_y; j++){
            for (int k=0; k<2 ; k++){
                _velocity[i][j][k]=0;
            }
            _massPerCell[i][j]=0;
            _particlesPerCell[i][j]=0;
            _angles[i][j]=0;
        }
    }
}

void Grid::resetAll(){
    reset();
    _steps= 0;
    for (int i=0; i<_n_x+1; i++){
        for (int j=0; j<_n_y; j++){
            for (int k=0; k<2 ; k++){
                _meanVelocity[i][j][k]=0;
            }
        }
    }
}

void Grid::initAngles(){
    if(TRUE_RANDOM_ANGLE){
        std::uniform_real_distribution<double> pi(-PI,PI);
        for (int i=0; i<_n_x+1; i++){
            for (int j=0; j<_n_y; j++){
                _angles[i][j]= pi(_generator); // random angle between -pi and pi
            }
        }
    }else{
        std::uniform_int_distribution<int> sign(0,1);
        for (int i=0; i<_n_x+1; i++){
            for (int j=0; j<_n_y; j++){
                _angles[i][j]= (sign(_generator) == 0) ? -ALPHA : ALPHA ; //random sign, fixed angle
            }
        }
    }
}
void Grid::eulerStep(){
    const int fraction = 100;
    for (int i= 0; i<fraction; i++){
        for (int j=0; j<_polymerCount ;j++ )
        {
            _polymers[j].eulerMidpoint(_time/fraction, _x_l, _x_r, _y_l, _y_r, Eigen::Vector2d(0, _pressure));
        }
    }
}

void Grid::simulateFlow(unsigned steps){
    std::uniform_real_distribution<double> dist(0,1);
    auto ran = std::bind(dist, _generator);
    calculateMeanSpeed(); // init.
    for (unsigned s=0; s<steps ;s++ ){
        _steps++;
        if(s%100==0 && steps>100){
            std::printf("progress: %d\n",s/100);
        }
        double x = (ran()-0.5);
        double y = (ran()-0.5);

        shiftGrid(x,y);
        if(TESTING){
            std::printf("shift params: x: %.4f, y: %.4f \n",x ,y );
            std::printf("# out of bounds during shift: %d\n", particlesOutsideOfBoundary());
        }
        if(TESTING){
            for (int k=0; k<_polymerCount; k++){
                _polymers[k].show();
            }
            std::printf("total momentum before srd: %.3f , %.3f\n", totalMomentum(0),totalMomentum(1));
        }
        srdStep();
        if(TESTING){
            std::printf("total momentum after srd: %.3f , %.3f\n", totalMomentum(0),totalMomentum(1));
        }
        if(TESTING){
            for (int k=0; k<_polymerCount; k++){
                _polymers[k].show();
            }
        }
        shiftGrid(-x,-y); // shift back.
        if(particlesOutsideOfBoundary()>0){
            std::printf("%d Paricles OOB after shift: This should not happen.\n", particlesOutsideOfBoundary()>0);
        }
        if(TESTING){
            std::printf("# out of bounds after backshift: %d\n", particlesOutsideOfBoundary());
        }

        if(TESTING){
            std::printf("total momentum before acc.: x,y: %.2f , %.2f\n", totalMomentum(0),totalMomentum(1));
        }
        //accelerationStep();  // no particle acceleration, only polymer acceleratrion.
        streamingStep();
        eulerStep();
        for (int i=0; i< _n_x+1; i++){
            for(int j=0; j< _n_y; j++){
                for(int k=0; k<2; k++){
                    _meanVelocity[i][j][k]+=_velocity[i][j][k];
                }
            }
        }
        if(TESTING){
            for (int k=0; k<_polymerCount; k++){
                _polymers[k].show();
            }
        }
        if(TESTING){
            std::printf("total momentum after acc.: x,y: %.2f , %.2f\n\n", totalMomentum(0),totalMomentum(1));
        }
    }
}

std::vector<Polymer>::iterator Grid::getPolymers(){
    return _polymers.begin();
}

int Grid::particlesOutsideOfBoundary(){
    //just for testing
    int counter= 0;
    for (int j= 0; j< _freeParticleCount; j++){
        if(!_particles[j].checkBorders(_x_l, _x_r, 0) || !_particles[j].checkBorders(_y_l, _y_r, 1) )
            counter++;
    }
    for (int j= 0; j< _polymerCount; j++){
        counter += _polymers[j].particlesOutsideOfBoundary(_x_l, _x_r, _y_l, _y_r);
    }
    return counter;
}

double Grid::totalMomentum(int which){
    // for testing
    double x=0, y=0;
    for (int k=0; k< _freeParticleCount; k++){
        x += _particles[k].getVel(0)* _particles[k].getMass();
        y += _particles[k].getVel(1)* _particles[k].getMass();

    }
    for (int k=0; k<_polymerCount ; k++)
    {
        std::vector<Particle>::iterator it=_polymers[k].getChain();
        for(int i=0; i< _polymers[k].getLength(); i++){
            x += it->getVel(0)* it->getMass();
            y += it->getVel(1)* it->getMass();
            it++;
        }
    }
    return (which==0)? x: y;
}

void Grid::buildFiles( ){
    calculateMeanSpeed(); // to make sure that the arrays are initialized.
    if(_steps >0){
        for (int i=0; i< _n_x+1; i++){
            for(int j=0; j< _n_y; j++){
                for(int k=0; k<2; k++){
                    _meanVelocity[i][j][k]/=_steps;
                }
            }
        }
    }
    FILE * pFile;
    std::printf("Building Files\n");
    pFile = fopen("data_particles.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%d\t", _particlesPerCell[i][j]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    pFile = fopen("data_mass.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%.3f\t", _massPerCell[i][j]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    pFile = fopen("data_meanV.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%.16f\t", _velocity[i][j][1]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    pFile = fopen("data_mittelVy.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%.16f\t", _meanVelocity[i][j][1]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    pFile = fopen("data_mittelVx.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%.16f\t", _meanVelocity[i][j][0]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);
}
