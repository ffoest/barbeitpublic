//includes
#include "particle.h"
#include "grid.h"
#include "polymer.h"
#include<Eigen/Dense>
//math
#include <cmath>
#include <vector>
#include <functional>
//output
#include <cstdio>
#include <string>
#include <tuple>
#include <fstream>
#include <iostream>
#include <iomanip>

void polyTest();


std::tuple<Eigen::Vector2d, Eigen::Vector2d, Eigen::Vector2d, std::vector<double>, double, double> getData(Polymer& poly,const double yl,const double yr){
    Eigen::Vector2d cmx = poly.getCMPosition(yl, yr);
    Eigen::Vector2d cmv = poly.getCMVelocity();
    Eigen::Vector2d e2e = poly.getEndToEnd(yl, yr);
    std::vector<double> cmVector = poly.getCMVector();
    double meanAngle = poly.getMeanAngle(yl, yr);
    double c = poly.getContour(yl, yr);
    return std::make_tuple(cmx, cmv, e2e, cmVector, meanAngle, c);
}

// streaming in y direction
int main()
{
//    /// TEST ///
//    {
//    polyTest();
//    return 0;
//    }
//    /// TEST ///

    // parameters of the simulation:
    const int INITTIME = 5000;     // 5k
    const int MEASURETIME = 20000;//10000;  // 10k
    const int STEP = 5;
    const unsigned SEED = 42;
    //grid

    const double TEMPERATURE = 0.01; // T=0.01
    const double timestep= 1;       // t=1
//    const double pressure= 0.01;    // p=0.01
    //particles

    const double particleMass= 1;
    //polymers
    const int polymerCount= 1;
    const int polymerLength = 10;
    const double bondLength= 1;
    const double monomerMass = 10 * particleMass ; //10
    const double COUPLING = 100 * monomerMass; // k/m = const

    const std::vector<int> GRID_WIDTH = {20};
    const std::vector<double> BEND = {0, 0.05, 0.1, 1 };//, 2, 3, 4, 5, 6, 0}; //should not be higher than 25*monomermass else simulation fails.
    const std::vector<double> PRESSURE = {0.00, 0.02, 0.04, 0.06, 0.08, 0.10, 0.12, 0.14};//, 0.12, 0.14, 0.16, 0.18, 0.2}; // times monomer mass// if much higher simulation fails.
    // init particles
    double sigma = std::sqrt(TEMPERATURE/particleMass);
    std::mt19937 generator(SEED);
    std::uniform_real_distribution<double> dist(0,1);
    std::normal_distribution<double> gauss(0.0,sigma);
    auto ran = std::bind(dist, generator);
    auto gaussian = std::bind(gauss, generator);

    // run the simulation for various grid sizes and bending forces.
    for (unsigned q=0; q<GRID_WIDTH.size(); q++){
        const int gridHeight= 500;   //y=100
        const int gridWidth= GRID_WIDTH[q];    //x=40
        const int particlesPerCell = 10; // 10
        const int particleCount= gridWidth*gridHeight*particlesPerCell;
        std::vector<Particle> part (particleCount);
        for (int i=0; i< particleCount; i++){
            part[i]= Particle( ran()*gridWidth+0.5 , ran()* gridHeight +0.5 , gaussian() , gaussian() , particleMass);
        }
        for (unsigned s=0; s< BEND.size(); s++){
            std::stringstream ss, ss2;
            ss << "data_basic_" << BEND[s] << "_" << GRID_WIDTH[q] << ".dat";
            ss2 << "data_hist_" << BEND[s] << "_" << GRID_WIDTH[q] << ".dat";
            std::ofstream ofs, ofs2;
            ofs.open (ss.str().c_str(), std::ofstream::out);
            ofs2.open (ss2.str().c_str(), std::ofstream::out);
            ofs << "# K: " << BEND[s] << '\n';
            ofs << "# Pressure\t K_Bend \t cos2(theta)\t e2eLength\t CMVelocity(x)\t CMVelocity(y)\t CMPosition(x) \t CMPosition(y)\t mAngle \t contour \n";
            ofs2 << "# Histogram, k= " << BEND[s] <<  " Pressure= {0.00, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.45}" << "\n";


//            std::stringstream ss ;
//            ss << "data_cme2e_" << BEND[s] << "_" << GRID_WIDTH[q] << ".dat";
//            std::ofstream ofs;
//            ofs.open (ss.str().c_str(), std::ofstream::out);
//            std::ofstream teststream;
//            if(TEST_ENABLED && s== BEND.size()-1){
//                teststream.open("data_testing.dat");
//                teststream << "# e2e\t angle\t CMpos(x) \t CMpos(y) \t CMvel(x)\n CMvel(y)\n";
//            }
//            ofs << "# KBend: " << BEND[s] << '\n';
//            ofs << "# Pressure\t cos^2(theta)\t e2eLength\t CMVelocity(x)\t CMVelocity(y)\t CMPosition(x) \t CMPosition(y)\n";
            #pragma omp parallel for ordered schedule(dynamic)
            for (unsigned r=0; r< PRESSURE.size(); r++){
                std::vector<Polymer> pol (polymerCount);
                for (int i=0; i< polymerCount; i++){
                    // use i as seed to get an independent seed for each polymer.
                    pol[i]= Polymer( polymerLength, bondLength, COUPLING, BEND[s]*monomerMass, monomerMass, i);
                    pol[i].initializeOrdered((gridWidth-polymerLength)/2+0.5, gridHeight/2, /*ran()*gridWidth+0.5 , ran()* gridHeight +0.5*/  0.5, 0.5+ gridWidth, 0.5, 0.5+ gridHeight, TEMPERATURE);
                }
                double angle=0;
                double e2eLength=0;
                Eigen::Vector2d CMV = Eigen::Vector2d::Zero();
                Eigen::Vector2d CMX = Eigen::Vector2d::Zero();
                int precision = 2; // number of bins
                std::vector<double> CMDist(precision*(gridWidth+1));
                double mAngle = 0;
                double contourLength = 0;

                // initialization
                Grid grid(gridWidth, gridHeight, timestep, monomerMass*PRESSURE[r], TEMPERATURE);
                grid.populate(part); //init particles
                grid.populate(pol); //init polymers
    //            std::printf("total Mass: %.2f\n",grid.getTotalMass());
    //            std::printf("OOB: %d\n", grid.particlesOutsideOfBoundary());

                grid.simulateFlow(INITTIME);
                grid.resetAll();
                // simulation
                for (int i=0; i<MEASURETIME; i++){
                    grid.simulateFlow(STEP); // 10 steps
                    std::vector<Polymer>::iterator poly = grid.getPolymers();
                    Eigen::Vector2d cmPosition,cmVelocity, endToEnd;
                    std::vector<double> cmVector;
                    double meanAngle, cont;
                    std::tie(cmPosition, cmVelocity, endToEnd, cmVector, meanAngle, cont) = getData(*poly, 0.5, 0.5+gridHeight);
                    angle += pow(std::cos(std::atan2(endToEnd(0), endToEnd(1))),2);
                    e2eLength += endToEnd.norm();
                    CMX += cmPosition;
                    CMV += cmVelocity;
                    mAngle += meanAngle;
                    contourLength += cont;
                    for (unsigned j=0; j< cmVector.size(); j++){
                        int pos = cmVector[j]*2; // implicit cast
                        CMDist[pos] ++;
                    }
                }
                #pragma omp ordered
                {
                    ofs << PRESSURE[r] << '\t' <<BEND[s] << '\t' ;
                    //write data:
                    ofs << angle/MEASURETIME << '\t' << e2eLength/MEASURETIME << '\t';
                    ofs << CMV(0)/MEASURETIME << '\t' << CMV(1)/MEASURETIME << '\t' ;
                    ofs << CMX(0)/MEASURETIME << '\t' << CMX(1)/MEASURETIME << '\t';
                    ofs << mAngle/MEASURETIME << '\t' << contourLength/MEASURETIME << '\t';
                    for (unsigned i=0; i<CMDist.size(); i++){
                        ofs2 << CMDist[i]/MEASURETIME << '\t';
                    }
                    ofs << '\n';
                    ofs2 << '\n';
                }
                if(s==BEND.size()-1 && r==PRESSURE.size()-1 && q== GRID_WIDTH.size()-1){
                    grid.buildFiles(); // testing purpose
                }
            }
            ofs.close();
            ofs2.close();
        }
    }
//    polyTest();
    std::printf("Finished!\n");
    return 0;
}

void polyTest(){
    //
    //  TESTING
    //
    const int INITTIME = 2000;     // 5k
//    const int MEASURETIME = 100000;  // 100k
//    const int STEP = 10;  // # of steps per measurement
    const unsigned SEED = 42;

    const int gridHeight= 500;   //y=100
    const int gridWidth= 20;    //x=40
    const int particlesPerCell = 10; // 10
    const int particleCount= gridWidth*gridHeight*particlesPerCell;

    const double timestep = 1;
    const double particleMass= 1;
    const double TEMPERATURE = 0.01;
    //polymers
    const int polymerCount= 1;
    const int polymerLength = 10;
    const double bondLength= 1;
    const double monomerMass = 10 * particleMass ; //10
    const double COUPLING = 100 * monomerMass; // k/m = const = 100

    const std::vector<double> BEND = {0, 0.1, 1};
    const std::vector<double> PRESSURE = {0.02, 0.04, 0.08, 0.12};//, 0.6 }; // if much higher simulation fails.

    double sigma = std::sqrt(TEMPERATURE/particleMass);
    std::mt19937 generator(SEED);
    std::uniform_real_distribution<double> dist(0,1);
    std::normal_distribution<double> gauss(0.0,sigma);
    auto ran = std::bind(dist, generator);
    auto gaussian = std::bind(gauss, generator);
    std::vector<Particle> part (particleCount);
    for (int i=0; i< particleCount; i++){
        part[i]= Particle( ran()*gridWidth+0.5 , ran()* gridHeight +0.5 , gaussian() , gaussian() , particleMass);
    }
    for (unsigned s=0; s< BEND.size(); s++){
        std::vector<Polymer> pol (polymerCount);
        for (int i=0; i< polymerCount; i++){
            // use i as seed to get an independent seed for each polymer.
            pol[i]= Polymer( polymerLength, bondLength, COUPLING, BEND[s]*monomerMass, monomerMass, i);
            pol[i].initializeOrdered((gridWidth-polymerLength)/2+0.5, gridHeight/2, /*ran()*gridWidth+0.5 , ran()* gridHeight +0.5*/  0.5, 0.5+ gridWidth, 0.5, 0.5+ gridHeight, TEMPERATURE);
        }
        std::ofstream ofs;
        ofs  << std::fixed << std::setprecision(2);
        std::stringstream ss;
        ss << "data_polymer_positions_" << BEND[s] << "_" << gridWidth << ".dat" ;
        ofs.open (ss.str().c_str(), std::ofstream::out);

        #pragma omp parallel for ordered schedule(dynamic)
        for (unsigned r=0; r< PRESSURE.size(); r++){
            // initialization
            Grid grid(gridWidth, gridHeight, timestep, PRESSURE[r], TEMPERATURE);
            grid.populate(part); //init particles
            grid.populate(pol); //init polymers
            std::printf("total Mass: %.2f\n",grid.getTotalMass());
            std::printf("OOB: %d\n", grid.particlesOutsideOfBoundary());
            grid.simulateFlow(INITTIME);
            grid.resetAll();
            // simulation
//            for (int i=0; i<MEASURETIME; i++){
//                grid.simulateFlow(STEP); // 10 steps
//                 }
            #pragma omp ordered
            {
                std::vector<Polymer>::iterator poly = grid.getPolymers();
                poly->show(ofs);
            }
            if(s==BEND.size()-1 && r==PRESSURE.size()-1){
            //    grid.buildFiles(); // testing purpose
            }
        }
        ofs.close();
    }
}

