/*
 *  Particle
 */
// Guard
#ifndef PARTICLE_INCLUDED
#define PARTICLE_INCLUDED
#include <Eigen/Dense>
class Particle{
private:
    double _x, _y ;
    double _vx , _vy;
    double _mass;
public:
    Particle();
    Particle(double, double, double, double, double);       // Constructor
    ~Particle();                                            // Destructor
    double getPos (int);                                    // get coordinates
    double getVel (int);                                    // get velocity
    double getMass();                                       // get Particle Mass
    void streaming (double);                                // x+= v
    void accelerate (double, double, double);               // v+= F/m
    void srd (double, double, double);                      // SRD step
    bool checkBorders(double, double, int);                 // return true if a<x<b
    void verifyPeriodicBoundary(double, double);            // shift Particle until a<x<b
    void shift(double, double);                             // x+= x_0
    void reverseDir();                                      // v= -v
    double distance(Particle, const double);                // return distance to Particle p
    Eigen::Vector2d distanceVector(Particle,  const double);// return Vector to Particle p
};

#endif // PARTICLE_INCLUDED
