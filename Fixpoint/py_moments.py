# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as ml
from matplotlib import rcParams
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

gLength = 50 # Grid length
colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkslateblue', 'darkorange']
grid = [20,40]
# param = [0, 0.1, 1]
param = [1, 5, 10]
press = [0.00, 0.005, 0.01, 0.03, 0.05]

fig, axes = plt.subplots(len(param), len(grid), sharex='col', sharey='row', figsize= (8, 5.0)) 
# figsize in inch (a4 format)
for k in np.arange(0,len(grid)):
	for i in np.arange(0,len(param)):
		# axes labels
		axes[i,0].set_ylabel(r"$x/a$")	
		# axes[len(param)*len(grid)-1].set_xlabel(r"$x/a$")	
		
		# axes limits
		axes[i,k].set_aspect('equal')
		axes[i,k].set_xlim([0,gLength])
		# axes[i,k].set_ylim([0,grid[i,k]])
		
		# plot
		file = "data_polymer_positions_" + str(param[i]) + '_' + str(grid[k]) + ".dat"
		polyarray = np.loadtxt(file, unpack=False)
		nPress = polyarray.shape[0]
		# pltlabel = r"$\kappa_b= {0:.0f}\,\frac{{m a2}}{{\delta t^2}}$".format(param[i])
		for n in np.arange(0,nPress,2):
			pltlabel = r"$ \frac{{F}}{{m}} = {0:.2f}\,\frac{{a}}{{\delta t^2}}$".format(press[n])
			y= 	polyarray[n, 0::4]
			x= 	polyarray[n, 1::4]
			x = gLength -x
			dx = np.mean(x)
			dy = np.mean(y)
			if(np.max(x)-np.min(x) > gLength/2):
				dx = 0 # periodic BC
			x = (x - dx + (n+1)*(gLength/nPress)*0.85)%gLength 
			y = y - dy
			vy= polyarray[n, 2::4]
			vx= polyarray[n, 3::4]
			# print(x)
			axes[i,k].quiver(x, y, vx, vy, color = 'black', units = 'xy', pivot = 'tail',  scale_units = 'xy', scale = 0.5, width = 0.2, zorder = 2)
			axes[i,k].plot(x, y, '-o', color = colors[n], markersize=6, label= pltlabel, zorder = 1)

			text = r"$ d = {0:.0f}\,a\,;\,\frac{{\kappa_\mathrm{{b}}}}{{m_\mathrm{{m}}}} = {1:.1f}\,\frac{{a^2}}{{\delta t^2}}$".format(grid[k], param[i])
			axes[i,k].text(12, 6.8, text , fontsize=11, zorder = 3)

		axes[i,k].xaxis.set_ticks_position('bottom')
		# yticks = axes[i,k].yaxis.get_majorticklocs()
		# axes[i,k].yaxis.set_ticks(yticks[0:len(yticks)-1])
# legend
handles, labels = axes[0,0].get_legend_handles_labels()	
fig.legend( handles, labels, loc = (0.21, 0.02), ncol =4, prop={'size': 13} )
# layout
fig.tight_layout()
fig.subplots_adjust(left=0.1, bottom=0.18, right=0.95, top=None, hspace=0.02 , wspace=None) 	
# savefig
name = 'plot_fixed_freeze_all'
fig.savefig(name + '.png')
fig.savefig('../Latex/Abbildungen/' + name + '.pdf')
