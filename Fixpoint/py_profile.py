#encoding: utf-8
import numpy as np
import matplotlib as ml
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

arry = np.loadtxt("data_mittelVy.txt", unpack=True)

NY = arry.shape[1]
NX = arry.shape[0]
ymin=0
ymax = ymin + NY -1

x= np.linspace(0,NY-1,NY)
mean = np.zeros(NY)
for i in range(NX):
	mean += arry[i]

mean/= float(NX)
	
def f(x, a, b, c):
   return a*(x-b)**2+c

popt, pcov = curve_fit(f, x, mean)
print(popt)
print(pcov)
plt.title(r'Hagen Poiseuille')
plt.plot(x, mean, 'rx', label= r"Mittlere Geschwindigkeit")
plt.plot(x,f(x,popt[0], popt[1], popt[2]), 'b-', label = "Fit")
#plt.axis([ymin, ymax, 0, np.max(arry)+0.05]) 
plt.legend(loc='best')

plt.xlabel(r"Cell") 
plt.ylabel(r"$\bar v_y$",rotation=0) 
plt.savefig('plot_profile.png')
plt.savefig('plot_profile.pdf')