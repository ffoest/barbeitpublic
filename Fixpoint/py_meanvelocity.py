#encoding: utf-8
import numpy as np
import matplotlib as ml
import matplotlib.pyplot as plt

# # plt.subplot(1, 1, 1)
# # plt.title("p=0.1")
# arr = np.loadtxt("data_mittelVy.txt")
# NX= arr.shape[0]
# NY= arr.shape[1]
# # plt.pcolor()
# # plt.colorbar()

# fig = plt.figure(figsize=(8, 3.2))

# ax = fig.add_subplot(111)
# ax.set_title('Gemittelte Geschwindigkeit in y Richtung')
# ax.set_aspect('equal')
# ax.set_xlim(0, NY)
# ax.set_ylim(0, NX)

# plt.pcolor(arr)
# # cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
# # cax.get_xaxis().set_visible(False)
# # cax.get_yaxis().set_visible(False)
# # cax.patch.set_alpha(0)
# # cax.set_frame_on(False)
# plt.colorbar(orientation='vertical')
# plt.savefig('plot_mittelVy.png')
# plt.savefig('plot_mittelVy.pdf')


arrx = np.loadtxt("data_mittelVx.txt")
arry = np.loadtxt("data_mittelVy.txt")

xmin=0
NX = arrx.shape[0]
xmax = xmin + NX
ymin=0
NY = arrx.shape[1]
ymax = ymin + NY

fig = plt.figure(figsize=(7, 4))
ax = fig.add_subplot(111)
norm  = np.sqrt(arrx**2 + arry**2)
ax.quiver(arry / norm, arrx/ norm)

# ax.set_title('abs(mean(v)) per cell')
ax.set_aspect('equal')
# norm = np.array(np.split(norm, x[-1] + 1)).T

im = ax.imshow(norm, origin="lower")
cb = fig.colorbar(im, ax = ax, orientation="vertical")
#########
# cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8]) 
# cb = plt.colorbar(ax, cax = cbaxes)  
#########
cb.set_label(r"$v_{c,\mathrm{cm}} / \frac{a}{\delta t}$")
plt.xlabel(r"$z/a$") 
plt.ylabel(r"$x/a$") 
# plt.show()
fig.tight_layout()
fig.savefig("plot_quiver_fixpoint.png")
fig.savefig("../Latex/Abbildungen/plot_quiver_fixpoint.pdf")
