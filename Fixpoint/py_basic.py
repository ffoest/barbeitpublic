﻿# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as ml
from matplotlib import rcParams
# rcParams['text.usetex'] = True
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkorange', 'darkslateblue']
grid = [20, 40]
param = [1, 5, 10, 20]
nRows = 5
# fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8), (ax9, ax10), (ax11, ax12)) = plt.subplots(nRows, 2, sharex='col', figsize= (8.2, 11.0)) 
fig, axes = plt.subplots(nRows, 2, sharex='col', figsize= (8.2, 11.0)) 
# figsize in inch (a4 format)
# axes = ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8), (ax9, ax10), (ax11, ax12))
for k in range(len(grid)):
	# axes labels	
	axes[nRows-1][k].set_xlabel(r"$\frac{F}{m}\left[\frac{a}{\delta t^2}\right]$")
	axes[0][k].set_ylabel(r"$\langle L_\mathrm{E}/L_\mathrm{C}\rangle$")	
	axes[1][k].set_ylabel(r"$\langle X_\mathrm{EE}\rangle$")	
	axes[2][k].set_ylabel(r"$\langle X_{\mathrm{mean}}\rangle$")	
	axes[3][k].set_ylabel(r"$\langle \frac{F_\mathrm{tot,x}}{m_\mathrm{m}} / \frac{a}{\delta t^2}\rangle$") 
	axes[4][k].set_ylabel(r"$\langle \frac{F_\mathrm{tot,z}}{m_\mathrm{m}} / \frac{a}{\delta t^2}\rangle$")
	# axes[5][k].set_ylabel(r"$\frac{F}{m}\left[\frac{a}{\delta t^2}\right]$") 		
	for i in range(len(param)):
		# plot
		file = "data_basic_" + str(param[i]) + '_' + str(grid[k]) + ".dat"
		_p, _theta, _e2e, _vx, _vy, _x, _y, _contour, _mTheta, _fx, _fy = np.loadtxt(file, unpack=True)
		pltlabel = r"$\kappa_\mathrm{{b}}= {0:.0f}\,\frac{{m a^2}}{{\delta t^2}}$".format(param[i])
		axes[0][k].plot(_p, _e2e/_contour, '-o', color = colors[i], markersize=6, label= pltlabel)
		axes[1][k].plot(_p, _theta, '-o', color = colors[i], markersize=4, label= pltlabel)
		axes[2][k].plot(_p, _mTheta, '-o', color = colors[i], markersize=4, label= pltlabel)
		axes[3][k].plot(_p, _fx/10., '-o', color = colors[i], markersize=4, label= pltlabel)
		axes[4][k].plot(_p, _fy/10., '-o', color = colors[i], markersize=4, label= pltlabel)
		# axes[5][k].plot(_p, _y, '-o', color = colors[i], markersize=4, label= pltlabel)
	for i in range(nRows):
		if(i==0):
			text = r'$d= %3.0f a$'%(grid[k])
			axes[i][k].text(0.04, 0.85, text , fontsize=14)
		# axes limits
		axes[i][k].set_xlim([0,_p.max()*1.03])
		if(i<3):
			axes[i][k].set_ylim([0,1])
		# ticks
		axes[i][k].xaxis.set_ticks_position('bottom')
		yticks = axes[i][k].yaxis.get_majorticklocs()
		axes[i][k].yaxis.set_ticks(yticks[0:len(yticks)-1])
		if(i>2):
			axes[i][k].yaxis.set_ticks(yticks[0:len(yticks)-2])
		

# legend
handles, labels = axes[0][0].get_legend_handles_labels()	
fig.legend( handles, labels, loc = (0.12, 0.01), ncol =4, prop={'size': 13} )
# layout
fig.tight_layout()
fig.subplots_adjust(left=0.1, bottom=0.12, right=0.95, top=None, hspace=0.01 , wspace=None) 	
# savefig
name = 'plot_fixed_all'
fig.savefig(name + '.png')
fig.savefig(name + '.pdf')
fig.savefig('../Latex/Abbildungen/' + name + '.pdf')
# plt.close('all')
# plt.show()