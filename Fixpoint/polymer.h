//Guard
#ifndef POLYMER_INCLUDED
#define POLYMER_INCLUDED
#include "particle.h"
#include<Eigen/Dense>
#include<vector>
#include<random>
#include<fstream>

class Polymer
{
private:
    int _length;     // number of monomers in Polymer
    double _distance;// distance of equilibrium between monomers
    double _k;      // spring constant
    double _kBend;  // bending spring constant
    double _mass;   // monomer mass
    double _temp;   // temperature
    double _epsilon = 0.5; // Lennard Jones Energy
    double _LJsigma = 0.01; // lennard jones sigma/_distance
    double _fx; //fixpoint
    double _fy;
    int _index; // fixed monomer index
    std::mt19937 _generator;    // RN Generator
    std::vector<Particle> _chain;   // actual monomers

public:
    Polymer(int, double, double, double, double, int);      // constructor
    Polymer();                                      // standard constructor (necessary for array initialization)
    ~Polymer();                                     // destructor
    void show();                                    // text output of all positions and velocities of all monomers
    void show(std::ofstream&);                                    // text output of all positions and velocities of all monomers
    void initializeRandom(double, double, double, double, double, double, double);    // initialize polymer chain with monomers. the starting angles of all bonds are random.
    void initializeOrdered(double, double, double, double, double, double, double);    // initialize polymer chain with monomers. the starting angles of all bonds are zero (stiff).
    double totalEnergy(const double, const double);                             // E= 1/2 m v^2 + 1/2 k (x- x_0)^2 +LJ +Bend
    int particlesOutsideOfBoundary(double, double, double, double);             // return number of particles out of limits
    void eulerMidpoint(double, double, double, double, double, Eigen::Vector2d);// Euler Integration of Newtons equations
    void shift(double, double, double, double);                                 // shift entire polymer by x_0, check periodic boundary conditions.
    std::vector<Particle>::iterator getChain();       // return iterator to monomer Chain. The chain can be accessed from outside.
    std::vector<Eigen::Vector2d> calcForces(Eigen::Vector2d, double, double);  // calculate Forces for each monomer in chain.
    std::vector<Eigen::Vector2d> calcDistances(double, double);  // calculate distances for each monomer in chain.
    int getLength();                                            // get number of monomers.
    int getIndex();
    Eigen::Vector2d getEndToEnd(double, double) ;              // get End to End distance.
    Eigen::Vector2d getCMVelocity() ;                     // Center of mass velocity
    Eigen::Vector2d getCMPosition(double, double) ;         // get position of the center of mass of the polymer
    double getContour(double, double); // get contour length
    void fixPosition();
    double getMeanAngle(double, double); // returns 1/N * \sum [cos^2( \theta_i ) ]
    Eigen::Vector2d getFixpointForce(double, double); // get Force on fixed Monomer in current configuration
};

#endif // POLYMER_INCLUDED
