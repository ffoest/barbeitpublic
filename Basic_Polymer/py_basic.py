﻿# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as ml
from matplotlib import rcParams
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkorange', 'darkslateblue']
grid = [20, 40]
param = [0, 0.1, 1]
nRows = len(param)
fig, axes = plt.subplots(nRows, 2, sharex='col', figsize= (8, 5.0)) 
# figsize in inch (a4 format)
for k in range(len(grid)):
	# axes labels	
	axes[nRows-1][k].set_xlabel(r"$\frac{F}{m}\left[\frac{a}{\delta t^2}\right]$")
	axes[0][k].set_ylabel(r"$\langle L_\mathrm{E}/L_\mathrm{C}\rangle$")	
	axes[1][k].set_ylabel(r"$\langle X_\mathrm{EE}\rangle$")	
	axes[2][k].set_ylabel(r"$\langle X_{\mathrm{mean}}\rangle$")	
	for i in range(len(param)):
		# plot
		file = "data_basic_" + str(param[i]) + '_' + str(grid[k]) + ".dat"
		_p, _k, _theta, _e2e, _vx, _vy, _x, _y, _mTheta, _contour = np.loadtxt(file, unpack=True)
		pltlabel = r"$\frac{{\kappa_\mathrm{{b}}}}{{m_\mathrm{{m}}}}= {0:.2f}\,\frac{{a^2}}{{\delta t^2}}$".format(param[i])
		axes[0][k].plot(_p, _e2e/_contour, '-o', color = colors[i], markersize=6, label= pltlabel)
		axes[1][k].plot(_p, _theta, '-o', color = colors[i], markersize=6, label= pltlabel)
		axes[2][k].plot(_p, _mTheta, '-o', color = colors[i], markersize=6, label= pltlabel)
	for i in range(nRows):
		if(i==0):
			text = r'$d= %3.0f a$'%(grid[k])
			axes[i][k].text(0.3, 0.2, text , fontsize=14, bbox=dict(facecolor='none', edgecolor='black', pad = 10.0))
		# axes limits
		axes[i][k].set_xlim([0,_p.max()*1.03])
		axes[i][k].set_ylim([0,1])
		# ticks
		axes[i][k].xaxis.set_ticks_position('bottom')
		yticks = axes[i][k].yaxis.get_majorticklocs()
		axes[i][k].yaxis.set_ticks(yticks[0:len(yticks)-1])
		
# legend
handles, labels = axes[0][0].get_legend_handles_labels()	
fig.legend( handles, labels, loc = (0.2, 0.01), ncol =4, prop={'size': 13} )
# layout
fig.tight_layout()
fig.subplots_adjust(left=0.1, bottom=0.24, right=0.95, top=None, hspace=0.01 , wspace=None) 	
# savefig
name = 'plot_free_basic'
fig.savefig(name + '.png')
fig.savefig(name + '.pdf')
fig.savefig('../Latex/Abbildungen/' + name + '.pdf')
# plt.close('all')
# plt.show()


# colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkorange', 'darkslateblue']
# grid = [20, 40]
# param = [0, 0.1, 1]
# press = [0.00, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.45]

# #3 subplots
# for q in range(len(grid)):
	# f, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
	
	# for i in range(len(param)):
		# file = "data_basic_" + str(param[i]) + '_' + str(grid[q]) + ".dat"
		# p, k, theta, e2e, vx, vy, x, y, mTheta, contour = np.loadtxt(file, unpack=True)
		# # ax1.plot(p, vy, '-o', color = colors[i], markersize=6, label= r"$\kappa_b=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
		# ax1.plot(p, e2e/contour, '-o', color = colors[i], markersize=6, label= r"$\kappa_b=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
		# ax2.plot(p, theta, '-x', color = colors[i], markersize=6, label= r"$\kappa_b=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
		# ax3.plot(p, mTheta, '-+', color = colors[i], markersize=6, label= r"$\kappa_b=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
	# for ax in [ax1, ax2, ax3]:
		# ax.set_ylim([0,1]) 
		# ax.yaxis.tick_left()
		# box = ax.get_position()
		# ax.set_position([box.x0, box.y0, box.width * 1.17, box.height])
		# # Put a legend to the right of the current axis
		# # ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		# ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), borderaxespad=0.2, prop={'size':9})
		# # legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0. ,prop={'size':8})
		# # ax.legend(loc='best',prop={'size':8})
	# ax1.set_ylabel(r"$\langle L_E/L_C\rangle$") 
	# ax2.set_ylabel(r"$\langle X_{EE}\rangle$") 
	# ax3.set_ylabel(r"$\langle X_{\mathrm{mean}}\rangle$")
	# ax3.set_xlabel(r"$\frac{F}{m}\left[\frac{a}{\delta t^2}\right]$") 	
	# name = 'plot_basic_' + str(grid[q])
	# f.tight_layout()
	# f.subplots_adjust(hspace = 0.1)
	# # fig = ml.pyplot.gcf()
	# # fig.set_size_inches(6,3.5)	
	# f.savefig(name + '.png')
	# #f.savefig(name + '.pdf')
	# f.savefig('../Latex/Abbildungen/' + name + '.pdf')
	# plt.close('all')	
	# # ax3.set_xlabel(r"$\frac{p}{m}$") 
	# # ax1.set_ylabel(r"$\langle |v_y| /\left(\frac{a}{\delta t}\right)\rangle$") 
	# # ax2.set_ylabel(r"$\langle|\vec E|/C\rangle$") 
	# # ax3.set_ylabel(r"$\langle\mathrm{cos}^2(\theta)\rangle$")
	# # ax2.set_ylim([0,1]) 
	# # ax3.set_ylim([0,1]) 

	# # for i in range(len(param)):
		# # file = "data_basic_" + str(param[i]) + '_' + str(grid[q]) + ".dat"
		# # p, k, theta, e2e, vx, vy, x, y, mTheta, contour = np.loadtxt(file, unpack=True)
		# # # ax1.plot(p, vy, color[j+i], label= r"$v_y(p);X\,$= " + str(grid[j])+ "\,k= "+ str(param[i]))
		# # ax1.plot(p, vy, '-o', color = colors[i], markersize=6, label= r"$\kappa_b=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
		# # ax2.plot(p, e2e/contour, '-o', color = colors[i], markersize=6, label= r"$k=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
		# # ax3.plot(p, theta, '-x', color = colors[i], markersize=6, label= r"$cos^2(\theta);k=$ "+ str(param[i]) + r"$\frac{ma^2}{\delta t^2}$")
		# # ax3.plot(p, mTheta, '-+', color = colors[i], markersize=6, label= r"$\langle cos^2(\theta_i) \rangle;k=$ "+ str(param[i]))
	# # for ax in [ax1, ax2, ax3]:
		# # ax.yaxis.tick_right()
		# # ax.legend(loc='best',prop={'size':8})
		
	# # name = 'plot_basic_' + str(grid[q])
	# # f.tight_layout()
	# # f.savefig(name + '.png')
	# # #f.savefig(name + '.pdf')
	# # f.savefig('../Latex/Abbildungen/' + name + '.pdf')
	# # plt.close('all')

# print("finished!")


