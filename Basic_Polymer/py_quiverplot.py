#encoding: utf-8
import numpy as np
import matplotlib as ml
import matplotlib.pyplot as plt

# Set limits and number of points in grid 
# xmax = 2.0  
# xmin = -xmax 
# NX = 50 
# ymax = 2.0 
# ymin = -ymax 
# NY = 50
# Make grid and calculate vector components 
arrx = np.loadtxt("data_mittelVx.txt")
arry = np.loadtxt("data_mittelVy.txt")
xmin=0
NX = arrx.shape[0]
xmax = xmin + NX
ymin=0
NY = arrx.shape[1]
ymax = ymin + NY
# x = np.linspace(xmin, xmax, NX) 
# y = np.linspace(ymin, ymax, NY) 
# X, Y = np.meshgrid(x, y) 
 
fig = plt.figure()
ax = fig.add_subplot(111)
ax = plt.quiver(arry,arrx)
plt.title(r'abs(mean(v)) per cell')

# S2 = X**2 + Y**2 # This is the radius squared 
# Bx = X/np.sqrt(S2)
# By = Y/np.sqrt(S2)
# plt.figure()

# plt.quiverkey(QP, 0.95, 1.05, 1.0, '1 mT', labelpos='N') 
# Set the left, right, bottom, top limits of axes 
dx = (xmax - xmin)/(NX - 1) 
dy = (ymax - ymin)/(NY - 1) 
# plt.axis([xmin-dx, xmax+dx, ymin-dy, ymax+dy]) 
# plt.title(r"first test") 
plt.xlabel(r"x (cm)") 
plt.ylabel(r"y (cm)") 
plt.show() 
