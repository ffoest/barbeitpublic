//Guard
#ifndef POLYMER_INCLUDED
#define POLYMER_INCLUDED

#include<Eigen/Dense>
#include<vector>
#include<random>
#include<fstream>
#include "particle.h"
class Polymer
{
private:
    int _length;     // number of monomers in Polymer
    double _distance;// distance of equilibrium between monomers
    double _k;      // spring constant
    double _kBend;  // bending spring constant
    double _mass;   // monomer mass
    double _temp;   // temperature
    double _epsilon = 0.01; // Lennard Jones Energy
    double _LJsigma = 1; // lennard jones sigma/_distance
    std::mt19937 _generator;    // RN Generator
    std::vector<Particle> _chain;   // actual monomers

public:
    Polymer(int, double, double, double, double, int);      // constructor
    Polymer();                                      // standard constructor (necessary for array initialization)
    ~Polymer();                                     // destructor
    void show();                                    // text output of all positions and velocities of all monomers
    void show(std::ofstream&);                                    // text output of all positions and velocities of all monomers
    void initialize(double, double, double, double, double, double, double);    // initialize polymer chain with monomers. the starting angles of all bonds are random.
    double totalEnergy(const double, const double);                             // E= 1/2 m v^2 + 1/2 k (x- x_0)^2
    int particlesOutsideOfBoundary(double, double, double, double);             // return number of particles out of limits
    void eulerMidpoint(double, double, double, double, double, Eigen::Vector2d);// Euler Integration of Newtons equations
    void shift(double, double, double, double);                                 // shift entire polymer by x_0, check periodic boundary conditions.
    std::vector<Particle>::iterator getChain();      // return iterator to monomer Chain. The chain can be accessed from outside.
    std::vector<Eigen::Vector2d> calcForces(Eigen::Vector2d, double, double);  // calculate Forces for each monomer in chain.
    std::vector<Eigen::Vector2d> calcDistances(double, double);  // calculate distances for each monomer in chain.
    int getLength();                                 // get number of monomers.
    Eigen::Vector2d getEndToEnd(double, double) ;             // get End to End distance.
    Eigen::Vector2d getCMVelocity() ;                   // get Center of mass velocity
    Eigen::Vector2d getCMPosition(double, double) ;     // get Center of Mass coordinates.
    std::vector<double> getCMVector(); // returns a vector with all x coordinates of all monomers (mass distribution)
    double getMeanAngle(double, double); // returns 1/N * \sum [cos^2( \theta_i ) ]
    double getContour(double, double); // get contour length
};

#endif // POLYMER_INCLUDED
