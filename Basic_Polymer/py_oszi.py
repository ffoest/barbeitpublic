#encoding: utf-8
import numpy as np
import matplotlib as ml
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

arr = np.loadtxt("data_log.dat", unpack=True)
x = np.linspace(0, arr.shape[1]-1, arr.shape[1])
plt.title(r'Kette mit 6 Elementen')
# plt.plot(x, arr[1], 'k-', label= r"y_1")
# plt.plot(x, arr[3], 'c-', label= r"y_2")

plt.plot(x, arr[0], 'r.', label= r"x_1")
plt.plot(x, arr[1], 'r-', label= r"y_1")
if(arr.shape[0]>2):
	plt.plot(x, arr[2], 'b.', label= r"x_2")
	plt.plot(x, arr[3], 'b-', label= r"y_2")
if(arr.shape[0]>4):
	plt.plot(x, arr[4], 'g.', label= r"x_3")
	plt.plot(x, arr[5], 'g-', label= r"y_3")
if(arr.shape[0]>6):	
	plt.plot(x, arr[6], 'y.', label= r"x_4")
	plt.plot(x, arr[7], 'y-', label= r"y_4")
if(arr.shape[0]>8):
	plt.plot(x, arr[8], 'c.', label= r"x_5")
	plt.plot(x, arr[9], 'c-', label= r"y_5")
if(arr.shape[0]>10):
	plt.plot(x, arr[10], 'm.', label= r"x_6")
	plt.plot(x, arr[11], 'm-', label= r"y_6")

# plt.plot(arr[0], arr[1], 'r-', label= r"y_1")
# plt.plot(arr[16], arr[17], 'b-', label= r"y_9")
# plt.plot(arr[32], arr[33], 'g-', label= r"y_17")
# plt.plot(arr[56], arr[57], 'y-', label= r"y_29")
# plt.plot(arr[72], arr[73], 'c-', label= r"y_37")
# plt.plot(arr[98], arr[99], 'm-', label= r"y_50")
#plt.axis([0, 200, 0, 200]) 
plt.legend(loc='best')

plt.xlabel(r"$t$") 
plt.ylabel(r"$x(t)$",rotation=0) 
plt.savefig('plot_oszi.png')
plt.savefig('plot_oszi.pdf')