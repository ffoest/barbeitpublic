#include "polymer.h"

#define _USE_MATH_DEFINES
#include <cmath>

#include <random>
#include <functional>

#include <cstdio>
#include <iostream>

// toggle various forces on and off:
const bool HOOK = true;
const bool BENDING = true;
const bool LENNARD_JONES = true;

Polymer::Polymer(int length, double d, double k, double kBend, double mass, int seed):
    _length(length), _distance(d), _k(k) , _kBend(kBend), _mass(mass), _generator(seed)
{
    _chain = std::vector<Particle>(length);
}

Polymer::Polymer(): _length(0) , _mass(0)
{
    _chain = std::vector<Particle>(0);
}

Polymer::~Polymer(){
}

std::vector<Eigen::Vector2d> Polymer::calcDistances(double _yl, double _yr){
    if(_length==1){
        return std::vector<Eigen::Vector2d>(0);
    }
    double _lattice = _yr- _yl;
    std::vector<Eigen::Vector2d> distances (_chain.size()-1, Eigen::Vector2d::Zero());
    if(_length>1){
        for (unsigned j=0; j<distances.size(); j++){
            distances[j] = _chain[j].distanceVector(_chain[j+1], _lattice);
        }
    }
    return distances;
}

std::vector<Eigen::Vector2d> Polymer::calcForces(Eigen::Vector2d external, double _yl, double _yr){
    std::vector<Eigen::Vector2d> distances = calcDistances(_yl, _yr);
    std::vector<Eigen::Vector2d> force(_chain.size(), Eigen::Vector2d::Zero()+ external);
    if(HOOK && _length>1){
        Eigen::Vector2d tempDistance1 = distances[0];
        double tnorm= tempDistance1.norm();
        tempDistance1.normalize();
        tempDistance1 *= tnorm-_distance; // F = k(x_0-x)
        force[0] += _k* tempDistance1;

        Eigen::Vector2d tempDistance2 = -distances[_length-2];
        tnorm= tempDistance2.norm();
        tempDistance2.normalize();
        tempDistance2 *= tnorm-_distance;
        force[_length-1] += _k* tempDistance2;

        for (int j=1; j<_length-1; j++){
            tempDistance1 = distances[j];
            tnorm= tempDistance1.norm();
            tempDistance1.normalize();
            tempDistance1 *= tnorm-_distance;

            tempDistance2 = -distances[j-1];
            tnorm= tempDistance2.norm();
            tempDistance2.normalize();
            tempDistance2 *= tnorm-_distance;

            force[j]= _k*(tempDistance1 + tempDistance2);
        }
    }

    if(BENDING && _length>= 3){ // probably now finished
        for (int i=0; i<_length-2; i++){
            force[i] += _kBend * (distances[i]- distances[i+1]);
            force[i+1] += -2* _kBend* (distances[i]- distances[i+1]);
            force[i+2] += _kBend * (distances[i]- distances[i+1]);
        }
//        force[0] += _kBend * (distances[0]-distances[1]);
//        force[1] += (-2* _kBend * (distances[0]-distances[1]));
//        force[_length-1] += _kBend * (distances[_length-3]-distances[_length-2]);
    }

    if(LENNARD_JONES && _length > 1){
        double _lattice = _yr - _yl;
        double _sigma = _LJsigma* _distance; // these values are not final
        constexpr double _cutoff = pow(2., 1./6);
        for (int j=0; j<_length ; j++){
            for (int i=0; i<j; i++){
                double d = _chain[j].distance(_chain[i], _lattice);
                if(d/_sigma < _cutoff){
                    Eigen::Vector2d dVector = _chain[j].distanceVector(_chain[i], _lattice);
                    double f = -48./d *_epsilon*( pow(_sigma/d, -12)- 0.5* pow(_sigma/d, -6));
                    force[j]+= dVector.normalized()* f;
                    force[i]+= dVector.normalized()* (-f);
                }
            }
        }
    }

    return force;
}

void Polymer::eulerMidpoint(double _time, const double _xl, const double _xr, const double _yl, const double _yr, const Eigen::Vector2d external ){
    //init forces
    std::vector<Eigen::Vector2d> force_0 = calcForces(external, _yl, _yr);
    //std::printf("Distance(0): %.6f\n", _chain[0].distance(_chain[1], (_yr-_yl)));
    // r(t+h) = r(t) + h*v(t) + h^2 *F(t)/(2*m) + O(h^4);
    for(int j=0; j<_length; j++){
     //   std::cout << "Force (initial):" << j << "\n" << force_0[j] << '\n';
        double dx = 0.5*pow(_time,2)* force_0[j](0) / _mass;
        double dy = 0.5*pow(_time,2)* force_0[j](1) / _mass;
       // std::printf("dx: %.6f dy: %.6f", dx,dy);
        _chain[j].streaming(_time);
        if(!_chain[j].checkBorders(_xl, _xr, 0)){
            _chain[j].reverseDir();
            _chain[j].streaming(_time);
         //   std::printf("reversing direction!\n");
        }
        _chain[j].shift(dx,dy);
        if(!_chain[j].checkBorders(_xl, _xr, 0)){
            _chain[j].shift(-dx,-dy);
          //  std::printf("Particle still out of bounds. This should not happen.\n");
        }
    }
    std::vector<Eigen::Vector2d> force_1 = calcForces(external, _yl, _yr);
    // v(t+h) = v(t) + h (F(t)/m + F(t+h)/m)/2+ O(h^3)
    for(int j=0; j<_length; j++){
        force_1[j] = 0.5 * (force_0[j]+force_1[j]);
        //std::cout << "Force (final):" << j << "\n" << force_1[j] << '\n';
        _chain[j].accelerate(force_1[j](0), force_1[j](1), _time);
        _chain[j].verifyPeriodicBoundary(_yl, _yr);
    }
}

void Polymer::initialize(double sx, double sy, double _xl, double _xr, double _yl, double _yr, double t){
    std::normal_distribution<double> gauss(0.0,1.0);
    std::uniform_real_distribution<double> _2pi(0,2*M_PI);
    std::uniform_real_distribution<double> _angle(- 0.15*M_PI, 0.15*M_PI);
    auto gaussian = std::bind(gauss, _generator);
    // first particle goes to sx, sy.
    _chain[0]= Particle(sx, sy, std::sqrt(t/_mass)*gaussian(), std::sqrt(t/_mass)*gaussian(), _mass );
    for (int i=1; i<_length; i++){
        double tempx = sx; // begin at current position
        double tempy = sy;
        double tempphi = _2pi(_generator);
        do{
            double r= _distance+sqrt(t/_mass)*gaussian(); // random difference to mean bond length.
            tempphi += _2pi(_generator);     // free chain
//          tempphi += _angle(_generator);   // rotate angle only a bit.
            tempx = sx + r* cos(tempphi);
            tempy = sy + r* sin(tempphi);
        }while(tempx > _xr || tempx < _xl); //search new position until x=!OOB. y can be OOB because periodic BCondition
        sx = tempx; // write new position.
        sy = tempy;
        _chain[i]= Particle(sx, sy, std::sqrt(t/_mass)*gaussian(), std::sqrt(t/_mass)*gaussian(), _mass); //this might be OOB. not anymore
        _chain[i].verifyPeriodicBoundary(_yl, _yr);
    }
}

int Polymer::particlesOutsideOfBoundary(double _xl, double _xr, double _yl, double _yr){
    //just for testing
    int counter= 0;
    for (int j= 0; j< _length; j++){
        if(!_chain[j].checkBorders(_xl, _xr, 0) || !_chain[j].checkBorders(_yl, _yr, 1) )
            counter++;
    }
    return counter;
}

void Polymer::show(){
    std::printf("#Polymer:\n");
    for (std::vector<Particle>::iterator it=_chain.begin(); it!=_chain.end(); ++it){
        std::printf("%.4f\t%.4f\t%.4f\t%.4f\n", it->getPos(0), it->getPos(1), it->getVel(0), it->getVel(1));
    }
    std::printf("\n");
}

void Polymer::show(std::ofstream& ofs){
    ofs << "#Polymer:\n" ;
    for (std::vector<Particle>::iterator it=_chain.begin(); it!=_chain.end(); ++it){
        ofs << it->getPos(0) << '\t' << it->getPos(1) << '\t' << it->getVel(0) << '\t' << it->getVel(1) << '\t' ;
    }
    ofs << '\n';
}

double Polymer::totalEnergy(const double _yl, const double _yr){
    double energy= 0;
    std::vector<Eigen::Vector2d> distance = calcDistances(_yl, _yr);
    for(int j=0; j<_length; j++){
        if(_length >= 2 && j != _length-1 && HOOK){
            //   std::printf("Distance: %.5f \t Energy(k): %.5f \n", distance[j].norm(), 0.5*_k*pow(distance[j].norm()- _distance , 2));
            energy += 0.5*_k* pow(distance[j].norm()- _distance , 2); // 1/2 k(x-x_0)^2
            //  std::cout << "Energy (k): " << energy << '\n';
        }
        if(_length >= 3  && j < _length-2 && BENDING){
            energy += 0.5*_kBend* (distance[j]-distance[j+1]).squaredNorm();
        }
        energy += 0.5* _chain[j].getMass()*( pow(_chain[j].getVel(0), 2) + pow(_chain[j].getVel(1), 2 )); // 1/2 m v^2
    }
    if(LENNARD_JONES && _length > 1){
        double _lattice = _yr - _yl;
        double _sigma = _LJsigma* _distance;
        constexpr double _cutoff = pow(2., 1./6);
        for (int j=0; j<_length ; j++){
            for (int i=0; i<j; i++){
                double d = _chain[j].distance(_chain[i], _lattice);
                if(d/_sigma < _cutoff){
                    energy += 4 *_epsilon*( pow(_sigma/d, -12)- pow(_sigma/d, -6))+ _epsilon;
                }
            }
        }
    }
    return energy;
}

std::vector<Particle>::iterator Polymer::getChain(){
    return _chain.begin();
}

int Polymer::getLength(){
    return _length;
}

Eigen::Vector2d Polymer::getEndToEnd(double _yl, double _yr) {
    return _chain[0].distanceVector(_chain[_length-1], std::abs(_yr-_yl));
}

Eigen::Vector2d Polymer::getCMVelocity() {
    double vx=0;
    double vy=0;
    for(int i=0; i<_length; i++){
        vx+= _chain[i].getVel(0);
        vy+= _chain[i].getVel(1);
    }
    vx/= _length;
    vy/= _length;
    return Eigen::Vector2d(vx,vy);
}

Eigen::Vector2d Polymer::getCMPosition(double _yl, double _yr) {
    Eigen::Vector2d CM = Eigen::Vector2d(_chain[0].getPos(0),_chain[0].getPos(1));
    if(_length>1){
        Eigen::Vector2d temp = CM;
        std::vector<Eigen::Vector2d> distances = calcDistances(_yl, _yr);
        // all monomers have identical mass
        for(unsigned i=0; i<distances.size(); i++){
            temp += distances[i];
            CM+= temp;
        }
    }
    CM/= _length;
    if(CM(1)>_yr){
        CM(1) -= (_yr-_yl);
    }
    if(CM(1)<_yl){
        CM(1) += (_yr-_yl);
    }
    return CM;
}

void Polymer::shift(double x, double y, double _y_l, double _y_r){
    for (int j=0 ;j<_length ;j++ ){
        _chain[j].shift(x,y);
        _chain[j].verifyPeriodicBoundary(_y_l, _y_r);
    }
}

std::vector<double> Polymer::getCMVector(){
    std::vector<double> cmv(_length);
    for (int i=0; i<_length; i++){
        cmv[i] = _chain[i].getPos(0);
    }
    return cmv;
}

double Polymer::getMeanAngle(double _yl, double _yr){
    double res = 0;
    std::vector<Eigen::Vector2d> distances = calcDistances(_yl, _yr);
    for (unsigned i=0; i< distances.size(); i++){
        res += pow(std::cos(std::atan2(distances[i](0), distances[i](1))),2);
    }
    return res/distances.size();
}

double Polymer::getContour(double _yl, double _yr) {
    double contour = 0;
    if(_length>1){
        std::vector<Eigen::Vector2d> distances = calcDistances(_yl, _yr);
        for(unsigned i=0; i<distances.size(); i++){
            contour += distances[i].norm();
        }
    }
    return contour;
}
