#include "particle.h"
#include <cmath>
#include <algorithm>
Particle::Particle(double x, double y, double vx, double vy, double m):_x(x), _y(y), _vx(vx), _vy(vy), _mass(m)
{}
Particle::Particle(){}
Particle::~Particle(){}

double Particle::getPos(int which){
    switch(which){
        case 0: return _x;
        case 1: return _y;
        default: return 0;
    }
}

double Particle::getVel(int which){
    switch(which){
        case 0: return _vx;
        case 1: return _vy;
        default: return 0;
    }
}

double Particle::getMass(){
    return _mass;
}

void Particle::streaming(double time){
    _x += ( _vx * time ) ;
    _y += ( _vy * time ) ;
}

void Particle::reverseDir(){
    _vx=-_vx;
    _vy=-_vy;
}

void Particle::srd(double mean_vx, double mean_vy, double angle){
    double dx = _vx - mean_vx;
    double dy = _vy - mean_vy;
    _vx = mean_vx + dx* std::cos(angle) - dy* std::sin(angle);
    _vy = mean_vy + dx* std::sin(angle) + dy* std::cos(angle);
}

void Particle::accelerate(double f_x, double f_y, double time){
    _vx += f_x/_mass * time;
    _vy += f_y/_mass * time;
}

bool Particle::checkBorders(double a, double b, int which){
    switch(which){
    case 0: return _x >= a && _x < b; break;
    case 1: return _y >= a && _y < b; break;
    default: return false;
    }
}

void Particle::verifyPeriodicBoundary(double a, double b){
    while(_y<a){
        _y += (b-a);
    }
    while(_y>b){
        _y -= (b-a);
    }
}

void Particle::shift (double x, double y){
    _x+=x;
    _y+=y;
}

double Particle::distance(Particle p, const double lattice){
    double deltaX = std::abs(_x - p.getPos(0));
    double deltaY = std::abs(_y - p.getPos(1));
    deltaY = std::min(deltaY, std::abs(deltaY-lattice)); // periodic boundary
    return std::sqrt(std::pow(deltaX,2)+ std::pow(deltaY,2));
}

// return shortest distance
Eigen::Vector2d Particle::distanceVector(Particle p, const double lattice){
    double deltaX = p.getPos(0) -_x ;
    double deltaY = p.getPos(1) -_y ;
    // periodic boundary
    if(std::abs(deltaY) <= lattice/2.0){
        return Eigen::Vector2d(deltaX, deltaY) ;
    }
    if(std::abs(deltaY- lattice) < std::abs(deltaY)){
        return Eigen::Vector2d(deltaX, deltaY-lattice) ;
    }
    if(std::abs(deltaY+ lattice) < std::abs(deltaY)){
        return Eigen::Vector2d(deltaX, deltaY+lattice) ;
    }
    std::printf("#Error while computing Distance.\n");
    return Eigen::Vector2d(deltaX, deltaY);
}
