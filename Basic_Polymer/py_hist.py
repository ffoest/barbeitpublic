# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as ml
from matplotlib import rcParams
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

colors = ['b' , 'r', 'g', 'c', 'm', 'y', 'darkorange', 'darkslateblue']

grid  = [20, 40]
param = [0, 0.1, 1]
press = [0.00, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.45]

fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, sharex='col', figsize= (8.2, 10.6)) # figsize in inch (a4 format)
axes = ((ax1, ax2), (ax3, ax4), (ax5, ax6))
for k in range(len(grid)):
	for i in range(len(param)):
		# axes labels
		axes[i][0].set_ylabel(r"$\langle N\rangle$") 
		axes[2][k].set_xlabel(r"$x/a$")
		# plot
		file = "data_hist_" + str(param[i])+ '_' + str(grid[k]) + ".dat"
		arr = np.loadtxt(file)
		cut = 1
		for j in range(cut, arr.shape[0]):
			xmax = arr.shape[1]
			x = np.linspace(-xmax/4+0.5, xmax/4+0.5, xmax)	
			pltlabel = r"$F = %3.2f \,\frac{ma}{\delta t^2}$"%(press[j])
			axes[i][k].plot(x, arr[j], '-o' ,color = colors[j], markersize=4 , label= pltlabel)
		# text
		#if(k==0):
		text = r'$\frac{\kappa_\mathrm{b}}{m_\mathrm{m}}= %3.2f \,\frac{a^2}{\delta t^2}$'%(param[i])
		axes[i][k].text(np.min(x)+ xmax/40., np.max(arr[j]), text , fontsize=12)
		# axes limits
		axes[i][k].set_xlim([-arr.shape[1]/4+0.5, arr.shape[1]/4+0.5]) 
		axes[i][k].set_ylim([0, np.max(arr[j])*1.15]) 
		# ticks
		axes[i][k].xaxis.set_ticks_position('bottom')
		yticks = axes[i][k].yaxis.get_majorticklocs()
		axes[i][k].yaxis.set_ticks(yticks[0:len(yticks)-2])

# legend
handles, labels = ax1.get_legend_handles_labels()	
fig.legend( handles, labels, loc = (0.12, 0.01), ncol =4, prop={'size': 13} )
# layout
fig.tight_layout()
fig.subplots_adjust(left=0.1, bottom=0.14, right=0.95, top=None, hspace=0.01 , wspace=None) 	
# savefig
name = 'plot_free_histograms'
fig.savefig(name + '.png')
fig.savefig(name + '.pdf')
fig.savefig('../Latex/Abbildungen/' + name + '.pdf')
plt.close('all')
