#encoding: utf-8


import numpy as np
import matplotlib as ml
import matplotlib.pyplot as plt

# plt.subplot(1, 1, 1)
# plt.title("p=0.1")
arr = np.loadtxt("data_meanV.txt")
# plt.pcolor()
# plt.colorbar()

NX= arr.shape[0]
NY= arr.shape[1]
fig = plt.figure(figsize=(8, 3.2))

ax = fig.add_subplot(111)
ax.set_title('Vy Momentaufnahme')
ax.set_aspect('equal')
ax.set_xlim(0, NY)
ax.set_ylim(0, NX)
plt.pcolor(arr)
# cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
# cax.get_xaxis().set_visible(False)
# cax.get_yaxis().set_visible(False)
# cax.patch.set_alpha(0)
# cax.set_frame_on(False)
plt.colorbar(orientation='vertical')
plt.savefig('plot_meanV.png')
plt.savefig('plot_meanV.pdf')