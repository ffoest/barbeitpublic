#include "grid.h"
#include <random>
#include <functional>
#include <cstdio>

// some Parameters for testing:
const bool TESTING = false;
const bool GHOSTSENABLED = true;

// parameters
std::mt19937 generator(666);
std::uniform_real_distribution<double> dist(0,1);
std::normal_distribution<double> gauss(0.0,1.0);
auto ran = std::bind(dist, generator);
auto gaussian = std::bind(gauss, generator);
// Streaming in Y direction.

Grid::Grid(int n_x, int n_y, int a, int n, double t, double temp): _n_x(n_x), _n_y(n_y), _a(a), _count(n), _time(t), _temperature(temp), _generator(43) {
    _velocity = new double ** [_n_x+1];
    _meanVelocity = new double ** [_n_x+1];
    _particlesPerCell = new int * [_n_x+1];
    _angles = new double * [_n_x+1];
    for (int i= 0; i< _n_x+1; i++){
        _velocity[i] = new double * [_n_y];
        _meanVelocity[i] = new double * [_n_y];
        _particlesPerCell[i] = new int [_n_y];
        _angles[i] = new double [_n_y];
        for (int j= 0; j< _n_y; j++){
            _velocity[i][j] = new double [2];
            _meanVelocity[i][j] = new double [2];
            for(int k=0; k<2; k++){
                _velocity[i][j][k] = 0;
                _meanVelocity[i][j][k] = 0;
            }
        }
    }
    _particles = new Particle[_count];
    _x_l = 0.5*_a;
    _y_l = 0.5*_a;
    _x_r = _x_l + _n_x * _a ;
    _y_r = _y_l + _n_y * _a ;
    reset();

}

Grid::~Grid(){
    for (int i=0; i<_n_x+1; i++){
        for (int j=0; j< _n_y; j++){
            delete[] _velocity[i][j];
            delete[] _meanVelocity[i][j];
        }
        delete[] _velocity[i];
        delete[] _meanVelocity[i];
        delete[] _particlesPerCell[i];
        delete[] _angles[i];
    }
    delete[] _velocity;
    delete[] _meanVelocity;
    delete[] _particlesPerCell;
    delete[] _particles;
    delete[] _angles;
}

bool Grid::populate(double m, double t){
    for (int j=0;j<_count ;j++ )
    {
        _particles[j] = Particle( ran()*_n_x*_a+ 0.5*_a , ran()* _n_y *_a+ 0.5* _a ,std::sqrt(t)* gaussian()  ,std::sqrt(t)* gaussian()  , m);
    }
    return true;
}

void Grid::streamingStep(){
    for (int i=0 ; i<_count ; i++ )
    {
        _particles[i].streaming(_time);
        if(! _particles[i].checkBorders(_x_l, _x_r, 0)){
            _particles[i].reverseDir();
            _particles[i].streaming(_time);
        }
        if(! _particles[i].checkBorders(_y_l, _y_r, 1)){
            _particles[i].verifyPeriodicBoundary(_y_l, _y_r);
        }
    }
}

void Grid::calculateMeanSpeed(){
    reset();
    float mean= float(_count)/(_n_x*_n_y);
    int x, y;
    for (int k=0; k<_count ; k++)
    {
        x= int(_particles[k].getPos(0)/_a);
        y= int(_particles[k].getPos(1)/_a);
        if(y == _n_y){
            y=0;
        }
        _particlesPerCell[x][y]++;
        for (int w=0; w<2 ; w++ )
        {
            _velocity[x][y][w] += _particles[k].getVel(w);
        }
    }
    for (int i=0; i<_n_x+1; i++){
        for (int j=0; j< _n_y; j++){
            // if there are too few particles at the border, add 0K ghost particles.
            if((i==0 || i==_n_x) && _particlesPerCell[i][j]>0 && _particlesPerCell[i][j]< mean && GHOSTSENABLED){
                double sigma = std::sqrt((mean - _particlesPerCell[i][j]) * _temperature);
                std::normal_distribution<double> distribution(0, sigma);
                for (int k=0; k<2; k++){
                    _velocity[i][j][k] += distribution(_generator);
                    _velocity[i][j][k] /= mean;
                }
            }else if(_particlesPerCell[i][j] > 0){
                for (int k=0; k<2; k++){
                    _velocity[i][j][k] /= _particlesPerCell[i][j];
                }
            }
        }
    }
//    for (int i=0; i<_n_x+1; i++){
//        for (int j=0; j< _n_y; j++){
//            if((i==0 || i==_n_x) && _particlesPerCell[i][j]>0 && _particlesPerCell[i][j]< mean && GHOSTSENABLED){
//                //add ghosts ( ghost particles have T=0 and thus no velocity )
//                for (int k=0; k<2; k++){
//                    _velocity[i][j][k] /= mean;
//                }
//            }else if(_particlesPerCell[i][j] > 0){
//                for (int k=0; k<2; k++){
//                    _velocity[i][j][k] /= _particlesPerCell[i][j];
//                }
//            }
//        }
//    }
}

void Grid::setPressure(double p){
    _pressure = p;
}

void Grid::accelerationStep(){
    for (int j=0; j<_count ;j++ )
    {
        _particles[j].accelerate(0,_pressure,_time);
    }
}

void Grid::shiftGrid(double x, double y){
    for (int j=0 ;j<_count ;j++ ){
        _particles[j].shift(x,y);
        _particles[j].verifyPeriodicBoundary(_y_l, _y_r);
    }
}

void Grid::srdStep(){
    calculateMeanSpeed();
    initAngles();
    for (int k=0; k<_count ; k++)
    {
        int x, y;
        x= (int)(_particles[k].getPos(0)/_a);
        y= (int)(_particles[k].getPos(1)/_a);
        if(y == _n_y){
            y=0; //periodic boundary.
        }
        _particles[k].srd(_velocity[x][y][0], _velocity[x][y][1], _angles[x][y]);
    }
}

void Grid::reset(){
    for (int i=0; i<_n_x+1; i++){
        for (int j=0; j<_n_y; j++){
            for (int k=0; k<2 ; k++){
                _velocity[i][j][k]=0;
            }
            _particlesPerCell[i][j]=0;
            _angles[i][j]=0;
        }
    }
}

void Grid::initAngles(){
    if(TRUE_RANDOM_ANGLE){
        std::uniform_real_distribution<double> pi(-PI,PI);
        for (int i=0; i<_n_x+1; i++){
            for (int j=0; j<_n_y; j++){
                _angles[i][j]= pi(_generator); // random angle between -pi and pi
            }
        }
    }else{
        std::uniform_int_distribution<int> sign(0,1);
        for (int i=0; i<_n_x+1; i++){
            for (int j=0; j<_n_y; j++){
                _angles[i][j]= (sign(_generator) == 0) ? -ALPHA : ALPHA ; //random sign, fixed angle
            }
        }
    }
}

void Grid::simulateFlow(int first, int second){
    for (int i=0; i<first ;i++ ){
        if(i%100==0 && first>100){
            std::printf("progress: %d\n",i/100);
            if(true){
                std::printf("T = %.3f", 2*getKineticEnergy()/_count);
            }
        }


        double x = (ran()-0.5)* _a;
        double y = (ran()-0.5)* _a;

        shiftGrid(x,y);
        if(TESTING){
            std::printf("shift params: x: %.4f, y: %.4f \n",x ,y );
            std::printf("# out of bounds during shift: %d\n", particlesOutsideOfBoundary());
            std::printf("total momentum before srd: %.2f , %.2f\n", totalMomentum(0),totalMomentum(1));
        }
        srdStep();
        if(TESTING){
            std::printf("total momentum after srd: %.2f , %.2f\n", totalMomentum(0),totalMomentum(1));
        }
        shiftGrid(-x,-y); // shift back.
        if(TESTING){
            std::printf("# out of bounds after backshift: %d\n", particlesOutsideOfBoundary());
        }
        if(TESTING){
            std::printf("total momentum before acc.: x,y: %.2f , %.2f\n", totalMomentum(0),totalMomentum(1));
        }
        accelerationStep();
        if(TESTING){
            std::printf("total momentum after acc.: x,y: %.2f , %.2f\n\n", totalMomentum(0),totalMomentum(1));
        }
        streamingStep();

       // calculateMeanSpeed(); //for safety and testing purpose.

    }
    for (int i=0; i<second ;i++ ){
        if(i%100==0 && second>100){
            std::printf("measure progress: %d\n",i/100);
            if(true){
                std::printf("T = %.3f", 2*getKineticEnergy()/_count);
            }
        }
        double x = (ran()-0.5)* _a;
        double y = (ran()-0.5)* _a;

        shiftGrid(x,y);
        srdStep();
        shiftGrid(-x,-y); // shift back.
        accelerationStep();
        streamingStep();
        for (int i=0; i< _n_x+1; i++){
            for(int j=0; j< _n_y; j++){
                for(int k=0; k<2; k++){
                    _meanVelocity[i][j][k]+=_velocity[i][j][k];
                }
            }
        }
    }
    for (int i=0; i< _n_x+1; i++){
        for(int j=0; j< _n_y; j++){
            for(int k=0; k<2; k++){
                _meanVelocity[i][j][k]/=second;
            }
        }
    }
}
int Grid::particlesOutsideOfBoundary(){
    //just for testing
    int counter= 0;
    for (int j= 0; j< _count; j++){
        if(!_particles[j].checkBorders(_x_l, _x_r, 0) || !_particles[j].checkBorders(_y_l, _y_r, 1) )
            counter++;
    }
    return counter;
}

double Grid::totalMomentum(int which){
    // for testing
    double x=0, y=0;
    for (int k=0; k< _count; k++){
        x+= _particles[k].getVel(0);
        y+= _particles[k].getVel(1);

    }
    return (which==0)? x: y;
}

double Grid::getKineticEnergy(){
    double E= 0;
    for (int i= 0; i<_count ; i++){
        E += 0.5*(pow(_particles[i].getVel(0), 2)+ pow(_particles[i].getVel(1), 2)); //m=1
    }
    return E;
}

void Grid::buildFiles( ){
    calculateMeanSpeed(); // to make sure that the arrays are initialized.
    FILE * pFile;
    std::printf("Building Files\n");
    pFile = fopen("data_particles.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%d\t", _particlesPerCell[i][j]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    pFile = fopen("data_meanV.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%.16f\t", _velocity[i][j][1]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    pFile = fopen("data_mittelVy.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%.16f\t", _meanVelocity[i][j][1]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);

    pFile = fopen("data_mittelVx.txt","w");
    for(int i= 0; i< _n_x+1; i++){
        for(int j= 0; j< _n_y; j++){
            fprintf (pFile, "%.16f\t", _meanVelocity[i][j][0]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);
}
