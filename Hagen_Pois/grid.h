//Guard
#ifndef GRID_INCLUDED
#define GRID_INCLUDED
//Includes
#include "particle.h"
#include <random>
constexpr double PI = 4*std::atan(1);

class Grid
{
private:
    int _n_x , _n_y; //number of x, y grids
    int _a; // lattice constant
    Particle * _particles;
    int _count; //number of particles.
    double *** _velocity; // mean velocity per cell.
    int ** _particlesPerCell; // particles per cell
    double ** _angles; // rotation angles for srd.
    double _pressure; // pressure difference between exits.
    double _x_l, _x_r, _y_l, _y_r; // limits
    double _time; //timestep
    double _temperature;
    std::mt19937 _generator;
    double*** _meanVelocity; // mean velocity per cell averaged over measurement time
    const double ALPHA = PI*90/180; // 130 degree angle for SRD
    const bool TRUE_RANDOM_ANGLE = true; // determines whether the angle for SRD is chosen in [-pi,pi] or \pm ALPHA

public:
    Grid(int, int, int, int, double, double); // N_X, N_Y, a, N, t
    ~Grid();
    void setPressure(double);
    bool populate(double, double); //initialize Grid with particles
    void streamingStep();           //streams all particles with their current velocity
    void calculateMeanSpeed();      // calculate speed and number of particles in each cell (includes Ghost particles)
    void srdStep();                 // statistic rotation in each cell
    void accelerationStep();        // accelerates all particles by fixed amount
    void reset();                   // reset all Cells (mean velocity and particles per Cell)
    void initAngles();              // init Angles for srd step
    void addGhosts(double);         //
    void shiftGrid(double, double);
    double ghostParticle(double);
    void simulateFlow(int, int );
    //output files:
    void buildFiles();
    //for testing:
    int particlesOutsideOfBoundary();
    double totalMomentum(int);
    double getKineticEnergy();
};

#endif // GRID_INCLUDED
