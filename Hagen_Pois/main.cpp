#include <cstdio>
#include <cmath>
#include "particle.h"
#include "grid.h"

// parameters of the simulation:
const double timestep= 1;
const int latticeConstant= 1;
const int particleMass= 1;
const int gridWidth= 30;    //x
const int gridHeight= 100;   //y
const int particlesPerCell = 10;
const int particleCount= gridWidth*gridHeight*particlesPerCell;
const int TEMPERATURE = 1;
const float pressure= 0.01;
const int INITTIME = 5000;
const int MEASURETIME = 25000; //25k
// streaming in y direction

int main()
{
    Grid grid(gridWidth, gridHeight, latticeConstant , particleCount , timestep, 0);
    grid.populate(particleMass, TEMPERATURE);
    grid.setPressure(pressure);
    grid.calculateMeanSpeed();
    grid.simulateFlow(INITTIME, MEASURETIME);
    grid.buildFiles();
    std::printf("Finished!\n");
    return 0;
}

