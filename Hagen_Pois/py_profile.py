#encoding: utf-8
import numpy as np
import matplotlib as ml
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import *
arry = np.loadtxt("data_mittelVy.txt", unpack=True)

NY = arry.shape[1]
NX = arry.shape[0]
ymin=0
ymax = ymin + NY -1
gradp= 0.1

x= np.linspace(0,NY-1,NY)
mean = np.zeros(NY)
for i in range(NX):
	mean += arry[i]

mean/= float(NX)
	
def f(x, a, b, c):
   return a*(x-b)**2+c
popt, pcov = curve_fit(f, x, mean)
(a, b, c) = correlated_values(popt, pcov)
print(a)
print(b)
print(c)
eta1 = -1/(2*a)*gradp
eta2 = (ymax**2)/(8*c)*gradp
print(r"eta =" + str(eta1))
print(r"eta =" + str(eta2))
# plt.title(r'Hagen Poiseuille')
plt.plot(x, mean, 'g+', label= r"Messwerte")
xr= np.linspace(0, ymax)
plt.plot(xr,f(xr,popt[0], popt[1], popt[2]), 'b-', label = "Fit mit Polynom 2. Grades")
plt.axis([ymin-.5, ymax+.5, 0, np.max(arry)+0.02]) 
plt.legend(loc='best')
plt.xlabel(r"$x/a$") 
plt.ylabel(r"$\langle u_{\mathrm{c},z}/ \frac{a}{\delta t} \rangle$",rotation=90) 
fig = ml.pyplot.gcf()
fig.set_size_inches(8,4)
# plt.savefig('plot_profile.png')
fig.tight_layout()
fig.savefig('plot_profile.png')
# fig.savefig('../Latex/Abbildungen/plot_profile.pdf')
