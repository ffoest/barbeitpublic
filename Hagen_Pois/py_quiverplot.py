#encoding: utf-8
import numpy as np
import matplotlib as ml
import matplotlib.pyplot as plt

arrx = np.loadtxt("data_mittelVx.txt")
arry = np.loadtxt("data_mittelVy.txt")
xmin=0
NX = arrx.shape[0]
xmax = xmin + NX
ymin=0
NY = arrx.shape[1]
ymax = ymin + NY
# x = np.linspace(xmin, xmax, NX) 
# y = np.linspace(ymin, ymax, NY) 
# X, Y = np.meshgrid(x, y) 

# ax.quiver(x, y, ux / norm, uy / norm, norm, cmap='spring')
# ax.set_xlabel("$x / a$")
# ax.set_ylabel("$y / a$")
# norm = np.array(np.split(norm, x[-1] + 1)).T
# im = ax.imshow(norm, origin="lower")
# cb = fig.colorbar(im, ax = ax, orientation="horizontal")
# cb.set_label(r"$v_{c,\mathrm{cm}} / \frac{a}{\updelta t}$")
# fig.savefig("quiver.pdf")

fig = plt.figure(figsize=(7, 4))
ax = fig.add_subplot(111)
norm  = np.sqrt(arrx**2 + arry**2)
ax.quiver(arry / norm, arrx/ norm, color = 'orange')

# ax.set_title('abs(mean(v)) per cell')
ax.set_aspect('equal')
# norm = np.array(np.split(norm, x[-1] + 1)).T

im = ax.imshow(norm, origin="lower")
cb = fig.colorbar(im, ax = ax, orientation="horizontal")
#########
# cbaxes = fig.add_axes([0.8, 0.1, 0.03, 0.8]) 
# cb = plt.colorbar(ax, cax = cbaxes)  
#########
cb.set_label(r"$|\vec u_{c}| / \frac{a}{\delta t}$")
plt.xlabel(r"$z/a$") 
plt.ylabel(r"$x/a$") 
# plt.show()
fig.tight_layout()
fig.savefig("plot_quiver.png")
# fig.savefig("../Latex/Abbildungen/plot_quiver.pdf")
