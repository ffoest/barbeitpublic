/*
 *  Particle
 *  FF
 */
// Guard
#ifndef PARTICLE_INCLUDED
#define PARTICLE_INCLUDED
class Particle{
private:
    double _x, _y ;
    double _vx , _vy;
    double _mass;
public:
    Particle();
    Particle(double, double, double, double, double);
    ~Particle();
    double getPos (int);
    double getVel (int);
    void streaming (double);
    void srd (double, double, double);
    void accelerate (double, double, double);
    bool checkBorders(double, double, int);
    void verifyPeriodicBoundary(double, double);
    void shift(double, double);
    void reverseDir();
};

#endif // PARTICLE_INCLUDED
